<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (Bitrix\Main\Loader::includeModule('igromafia.game')) {
    $iblockMap = Igromafia\Game\Config::getIblocName();
}
foreach ($arResult["POSTS"] as &$arPost) {
    list($arPost['IBLOCK_ID']) = explode('-', $arPost['CODE']);
    $arPost['TYPE'] = $iblockMap[$arPost['IBLOCK_ID']];
}
