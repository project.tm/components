$(function () {
    'use strict';

    $(document).on('click', '.message-list-contaner .edit-game .trigger', function () {
        $(this).closest('.messages-block').toggleClass('active');
    });

    $(document).on('click', ".edit-game .edit-game-message", function () {
        let href = $(this).closest('.messages-block').find('.top-line a').eq(1).attr('href');
        if (href) {
            document.location.href = href + '#0';
        }
    });

});