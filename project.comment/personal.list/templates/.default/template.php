<? if (empty($arResult['IS_AJAX'])) { ?>
    <div class="message-list-contaner">
    <? } ?>
    <?
    if ($arResult['FILTER']) {
        $APPLICATION->IncludeComponent(
                "project.comment:blog.new_posts.list", ".default", array(
            'WRAPPER' => $arResult['WRAPPER'],
            'FILTER' => array(
                'ID' => $arResult['FILTER']
            ),
            "MESSAGE_PER_PAGE" => "5",
            "PATH_TO_BLOG" => "/support/user/#blog#/",
            "PATH_TO_POST" => "/support/user/#blog#/#post_id#/",
            "PATH_TO_GROUP_BLOG_POST" => NULL,
            "PATH_TO_USER" => "/support/user/user/#user_id#/",
            "PATH_TO_BLOG_CATEGORY" => "/support/user/#blog#/?category=#category_id#",
            "PATH_TO_SMILE" => "/bitrix/images/blog/smile/",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "60",
            "BLOG_VAR" => NULL,
            "POST_VAR" => NULL,
            "USER_VAR" => NULL,
            "PAGE_VAR" => NULL,
            "DATE_TIME_FORMAT" => "H:i d.m.Y",
            "GROUP_ID" => "1",
            "SET_TITLE" => "N",
            "SEO_USER" => "N",
            "NAME_TEMPLATE" => "#NAME# #LAST_NAME#",
            "SHOW_LOGIN" => "N",
            "PATH_TO_CONPANY_DEPARTMENT" => NULL,
            "PATH_TO_SONET_USER_PROFILE" => NULL,
            "PATH_TO_MESSAGES_CHAT" => NULL,
            "PATH_TO_VIDEO_CALL" => NULL,
            "NAV_TEMPLATE" => "users",
            "POST_PROPERTY_LIST" => array(
            ),
            "USE_SHARE" => "N",
            "SHARE_HIDE" => NULL,
            "SHARE_TEMPLATE" => NULL,
            "SHARE_HANDLERS" => NULL,
            "SHARE_SHORTEN_URL_LOGIN" => NULL,
            "SHARE_SHORTEN_URL_KEY" => NULL,
            "SHOW_RATING" => "",
            "IMAGE_MAX_WIDTH" => "200",
            "IMAGE_MAX_HEIGHT" => "200",
            "ALLOW_POST_CODE" => "Y",
            "RATING_TYPE" => "",
            "COMPONENT_TEMPLATE" => ".default",
            "BLOG_URL" => "",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO",
            "NOT_FOUNT" => '<p class="attention">Комментарии еще не добавлены</p>',
                ), false
        );
    } else {
        ?>
        <p class="attention">Комментарии еще не добавлены</p>
        <? }
    ?>
    <? if (empty($arResult['IS_AJAX'])) { ?>
    </div>
<? } ?>