<div class="toolbar">
    <form action="" class="form-inline OrderFilterForm">
        <div class="views">
            <label>Вид списка:</label>
            <a href="<?= $arResult['arView']['card']['url'] ?>" data-name="view" data-value="card" class="sort-view-click <?= isset($arResult['arView']['card']['select']) ? 'grid-active' : 'grid' ?>"></a>
            <a href="<?= $arResult['arView']['line']['url'] ?>" data-name="view" data-value="line" class="sort-view-click <?= isset($arResult['arView']['line']['select']) ? 'list-active' : 'list' ?>"></a>
        </div>
        <div class="sort">
            <div class="sort-by">
                <label>Сортировать по:</label>
                <span class="select-box">
                    <select name="sort" onchange1="window.location.href = $(this).val();" class="sort-view-change">
                        <? foreach ($arResult['arSort'] as $key => $value) { ?>
                            <option value="<?= $value['url'] ?>" data-value="<?=$key ?>" <? if (isset($value['select'])) { ?>selected="selected"<? } ?> ><?= $value['name'] ?></option>
                        <? } ?>
                    </select>
                </span>
            </div>
        </div>
    </form>
    <div class="clear"></div>
</div>
<form method="get">
    <div class="filters">
        <div class="clear"></div>
    </div>
</form>