<?php

use Bitrix\Main\Loader,
    Project\Core\Model\FavoritesTable;

class ProjectProductListFavorites extends CBitrixComponent {

    public function executeComponent() {
        if (Loader::includeModule('project.core') and CUser::IsAuthorized()) {
            $userId = cUser::GetID();
        }

        $this->arResult['FILTER'] = array();
        $rsData = FavoritesTable::getList(array(
                    'select' => array('ELEMENT_ID'),
                    'filter' => array(
                        'TYPE' => $this->arParams['TYPE'],
                        'USER_ID' => $userId
                    ),
        ));
        $rsData = new CDBResult($rsData);
        while ($arItem = $rsData->Fetch()) {
            $this->arResult['FILTER'][] = $arItem['ELEMENT_ID'];
        }

        if (empty($this->arResult['FILTER'])) {
            return;
        }

        global $APPLICATION;
        $APPLICATION->IncludeComponent('project:product.list', 'top', array(
            'HEADER' => $this->arParams['HEADER'],
            'HEADER_URL' => $this->arParams['URL'],
            'FILTER' => array(
                'ID'=>$this->arResult['FILTER']
            ),
        ));
    }

}
