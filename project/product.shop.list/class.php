<?php

class ProjectProductShopList extends CBitrixComponent {

    public function executeComponent() {
        global $specFilter, $APPLICATION;
        $specFilter = $this->arParams['FILTER'];
        $arParams = array(
            'SHOW_ALL_WO_SECTION' => 'Y',
            'IBLOCK_TYPE' => 'content',
            'IBLOCK_ID' => '9',
            'ELEMENT_SORT_FIELD' => 'sort',
            'ELEMENT_SORT_ORDER' => 'asc',
            'ELEMENT_SORT_FIELD2' => 'id',
            'ELEMENT_SORT_ORDER2' => 'desc',
            'PROPERTY_CODE' =>
            array(
                0 => 'M_AGE',
                1 => 'M_JENRE',
                2 => 'M_GAMERS',
                3 => 'M_BONUS',
                4 => '',
            ),
            'PROPERTY_CODE_MOBILE' =>
            array(
            ),
            'META_KEYWORDS' => '-',
            'META_DESCRIPTION' => '-',
            'BROWSER_TITLE' => '-',
            'SET_LAST_MODIFIED' => 'N',
            'INCLUDE_SUBSECTIONS' => 'Y',
            'BASKET_URL' => '/shop/personal/cart/',
            'ACTION_VARIABLE' => 'action',
            'PRODUCT_ID_VARIABLE' => 'id',
            'SECTION_ID_VARIABLE' => 'SECTION_ID',
            'PRODUCT_QUANTITY_VARIABLE' => 'quantity',
            'PRODUCT_PROPS_VARIABLE' => 'prop',
            'FILTER_NAME' => 'specFilter',
            'CACHE_TYPE' => 'A',
            'CACHE_TIME' => '36000000',
            'CACHE_FILTER' => 'N',
            'CACHE_GROUPS' => 'Y',
            'SET_TITLE' => 'Y',
            'MESSAGE_404' => '',
            'SET_STATUS_404' => 'N',
            'SHOW_404' => 'N',
            'FILE_404' => NULL,
            'DISPLAY_COMPARE' => 'N',
            'PAGE_ELEMENT_COUNT' => $this->arParams['COUNT'] ?: '28',
            'LINE_ELEMENT_COUNT' => '3',
            'PRICE_CODE' =>
            array(
                0 => 'BASE',
            ),
            'USE_PRICE_COUNT' => 'Y',
            'SHOW_PRICE_COUNT' => '1',
            'PRICE_VAT_INCLUDE' => 'N',
            'USE_PRODUCT_QUANTITY' => 'N',
            'ADD_PROPERTIES_TO_BASKET' => 'Y',
            'PARTIAL_PRODUCT_PROPERTIES' => 'Y',
            'PRODUCT_PROPERTIES' =>
            array(
                0 => 'M_AGE',
                1 => 'M_JENRE',
                2 => 'M_GAMERS',
            ),
            'DISPLAY_TOP_PAGER' => 'N',
            'DISPLAY_BOTTOM_PAGER' => 'N',
            'PAGER_TITLE' => 'Товары',
            'PAGER_SHOW_ALWAYS' => 'N',
            'PAGER_TEMPLATE' => '.default',
            'PAGER_DESC_NUMBERING' => 'N',
            'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',
            'PAGER_SHOW_ALL' => 'N',
            'PAGER_BASE_LINK_ENABLE' => 'N',
            'PAGER_BASE_LINK' => NULL,
            'PAGER_PARAMS_NAME' => NULL,
            'OFFERS_CART_PROPERTIES' =>
            array(
                0 => 'M_PLATFORM',
                1 => 'M_WEBVERSION',
                2 => 'M_BONUS',
            ),
            'OFFERS_FIELD_CODE' =>
            array(
                0 => '',
                1 => '',
            ),
            'OFFERS_PROPERTY_CODE' =>
            array(
                0 => 'M_PLATFORM',
                1 => 'M_WEBVERSION',
                2 => '',
            ),
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_ORDER" => "asc",
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER2" => "desc",
            'OFFERS_LIMIT' => '5',
            'SECTION_ID' => NULL,
            'SECTION_CODE' => NULL,
            'SECTION_URL' => '/shop/catalog/#SECTION_CODE#/',
            'DETAIL_URL' => '/shop/catalog/#SECTION_CODE#/#ELEMENT_CODE#/',
            'USE_MAIN_ELEMENT_SECTION' => 'N',
            'CONVERT_CURRENCY' => 'N',
            'CURRENCY_ID' => NULL,
            'HIDE_NOT_AVAILABLE' => 'N',
            'HIDE_NOT_AVAILABLE_OFFERS' => 'N',
            'LABEL_PROP' =>
            array(
                0 => 'M_GAMERS',
                1 => 'M_JENRE',
                2 => 'M_AGE',
            ),
            'LABEL_PROP_MOBILE' =>
            array(
                0 => 'M_GAMERS',
                1 => 'M_JENRE',
                2 => 'M_AGE',
            ),
            'LABEL_PROP_POSITION' => 'top-left',
            'ADD_PICT_PROP' => '-',
            'PRODUCT_DISPLAY_MODE' => 'Y',
            'PRODUCT_BLOCKS_ORDER' => 'price,props,sku,quantityLimit,quantity,buttons,compare',
            'PRODUCT_ROW_VARIANTS' => '[{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false},{\'VARIANT\':\'2\',\'BIG_DATA\':false}]',
            'ENLARGE_PRODUCT' => 'STRICT',
            'ENLARGE_PROP' => '',
            'SHOW_SLIDER' => 'N',
            'SLIDER_INTERVAL' => '3000',
            'SLIDER_PROGRESS' => 'N',
            'OFFER_ADD_PICT_PROP' => '-',
            'OFFER_TREE_PROPS' =>
            array(
                0 => 'M_PLATFORM',
                1 => 'M_WEBVERSION',
            ),
            'PRODUCT_SUBSCRIPTION' => 'Y',
            'SHOW_DISCOUNT_PERCENT' => 'N',
            'DISCOUNT_PERCENT_POSITION' => 'bottom-right',
            'SHOW_OLD_PRICE' => 'Y',
            'SHOW_MAX_QUANTITY' => 'N',
            'MESS_SHOW_MAX_QUANTITY' => '',
            'RELATIVE_QUANTITY_FACTOR' => '',
            'MESS_RELATIVE_QUANTITY_MANY' => '',
            'MESS_RELATIVE_QUANTITY_FEW' => '',
            'MESS_BTN_BUY' => 'Купить',
            'MESS_BTN_ADD_TO_BASKET' => 'В корзину',
            'MESS_BTN_SUBSCRIBE' => 'Подписаться',
            'MESS_BTN_DETAIL' => 'Подробнее',
            'MESS_NOT_AVAILABLE' => 'Нет в наличии',
            'MESS_BTN_COMPARE' => 'Сравнение',
            'USE_ENHANCED_ECOMMERCE' => 'N',
            'DATA_LAYER_NAME' => '',
            'BRAND_PROPERTY' => '',
            'TEMPLATE_THEME' => 'blue',
            'ADD_SECTIONS_CHAIN' => 'Y',
            'ADD_TO_BASKET_ACTION' => NULL,
            'SHOW_CLOSE_POPUP' => 'Y',
            'COMPARE_PATH' => '/shop/catalog/compare.php?action=#ACTION_CODE#',
            'COMPARE_NAME' => NULL,
            'BACKGROUND_IMAGE' => '-',
            'COMPATIBLE_MODE' => 'Y',
            'DISABLE_INIT_JS_IN_COMPONENT' => 'N',
        );
        $APPLICATION->IncludeComponent("bitrix:catalog.section", $this->GetTemplateName(), $arParams);
    }

}
