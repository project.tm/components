<?php

use CIBlockElement,
    CDBResult,
    Bitrix\Main\Loader;

class ProjectBrandList extends CBitrixComponent {

    public function executeComponent() {
        if ($this->startResultCache(false, array($this->arParams))) {
            if (Loader::includeModule('project.core')) {
                $this->arResult['LIST'] = Project\Core\Api\Brand::getList();
                $this->arResult['LIST'] = array_chunk($this->arResult['LIST'], 5, true);
                $this->includeComponentTemplate();
            }
        }
    }

}
