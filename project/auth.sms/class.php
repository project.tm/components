<?php

use Bitrix\Main\Application,
    Bitrix\Main\Loader;

class ProjectAuthSms extends CBitrixComponent {

    public function executeComponent() {
        $this->arResult['ERROR'] = '';
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arResult['STEP'] = 'phone';
        if (Loader::includeModule('project.sms')) {
            $this->arResult['PHONE'] = trim($request->get('PERSONAL_PHONE'));
            if ($request->get('isPhone')) {
                if (empty($this->arResult['PHONE'])) {
                    $this->arResult['ERROR'] = 'Введите телефон';
                } else {
                    $phone = str_replace(array(' ', '(', ')', '-'), '', $this->arResult['PHONE']);
                    $is = preg_match('~(\+7[0-9]{10})~', $phone);
                    if ($is and strlen($phone) == 12) {
//                        pre($phone, $is, strlen($phone), $this->arResult['PHONE']);
                        $filter = array(
                            "ACTIVE" => "Y",
                            "PERSONAL_PHONE" => $this->arResult['PHONE']
                        );
                        $rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter);
                        if ($arUser = $rsUsers->Fetch()) {
                            $_SESSION[__CLASS__] = array(
                                'USER_ID' => $arUser['ID'],
                                'TIME' => time(),
                                'CODE' => randString()
                            );
                            pre($_SESSION[__CLASS__]);
                            $is = Project\Sms\Send::sms($phone, 'Авторизация на igromafia.ru: ' . $_SESSION[__CLASS__]['CODE']);
                            pre($is);
                            $this->arResult['STEP'] = 'sms';
                        } else {
                            $this->arResult['ERROR'] = 'Пользователь не найден';
                        }
                    } else {
                        $this->arResult['ERROR'] = 'Введите корректный телефон';
                    }
                }
            } elseif ($request->get('isSms')) {
                if (empty($_SESSION[__CLASS__]) or ( $_SESSION[__CLASS__]['TIME'] + Project\Sms\Config::TIME) < time()) {
                    $this->arResult['ERROR'] = 'Время сесии истекло';
                } else {
                    $this->arResult['STEP'] = 'sms';
                    $this->arResult['CODE'] = $request->get('CODE');
                    if ($_SESSION[__CLASS__]['CODE'] === $this->arResult['CODE']) {
                        global $USER;
                        $USER->Authorize($_SESSION[__CLASS__]['USER_ID']);
                        $this->arResult['STEP'] = 'auth';
                    } else {
                        $this->arResult['ERROR'] = 'Код не верный';
                    }
                }
            }
        } elseif ($_POST) {
            $this->arResult['ERROR'] = 'Ошибка сервера, повторите запрос позднее';
        }
        $this->includeComponentTemplate();
    }

}
