<div class="white-bg">
    <div class="wrapper">
        <div class="row top-games-block">
            <h2 class="center m20">топ 4 шутеров 2016</h2>
            <div class="top-games-wrap">
                <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="top-game-wrap">
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="top-game_wrap-img">
                            <img src="<?= $arItem['IMG'] ?>">
                        </a>
                        <div class="top-game-console-wrap">
                            <? foreach ($arItem['PROPERTY_PLATFORM_VALUE'] as $value) { ?>
                                <div class="console"><?= $value ?></div>
                            <? } ?>
                        </div>
                    </div>
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="top-game_game-name">
                        <?= $arItem['NAME'] ?>
                    </a>
                </div>
                <? } ?>
            </div>
        </div>
    </div>
</div>