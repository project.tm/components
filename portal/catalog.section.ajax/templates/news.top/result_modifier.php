<?

foreach ($arResult['ITEMS'] as &$arItem) {
    $img = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'];
    if ($img) {
        $arItem['IMG'] = Igromafia\Game\Image::resize($img, 106, 106);
    } else {
        $arItem['IMG'] = SITE_TEMPLATE_PATH .'head.png';
    }
}
unset($arItem);