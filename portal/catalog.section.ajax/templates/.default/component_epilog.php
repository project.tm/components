<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    $rating = (int)$request->getQuery('rating');
    if($rating and empty($arResult['RATING'][$rating])) {
        LocalRedirect($APPLICATION->GetCurPageParam("", array("rating")));
    }
    ob_start();
    if ($arParams['RATING_MAFIA']) {
        ?>
        <div class="section-mafia-rating">
            <div class="rat-star game-section-mafia-number">
                <? for ($i = 1; $i <= 10; $i++) { ?>
                    <div class="block-star section-rating-number <? if (empty($arResult['RATING'][$i])) { ?>rating-disabled<? } ?> <? if ($rating==$i) { ?>active<? } ?>"><?= $i ?></div>
                <? } ?>
            </div>
        </div>
        <?
    } else {
        ?>
        <div class="section-user-rating">
            <div class="number game-section-user-number">
                <? for ($i = 1; $i <= 10; $i++) { ?>
                    <div class="numbers-block section-rating-number <? if (empty($arResult['RATING'][$i])) { ?>rating-disabled<? } ?> <? if ($rating==$i) { ?>active<? } ?>"><?= $i ?></div>
                <? } ?>
            </div>
        </div>
        <?
    }
    ?>
    <input type="hidden" name="rating" class="filter-rating" value="<?=$request->getQuery('rating') ?>">
    <?
    $APPLICATION->SetPageProperty('catalog-ration', $content = ob_get_clean());
    ?>
    <?
    $jsParams = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "AJAX" => $arResult["AJAX"],
        "PARAM" => array(
            'SORT' => $arParams['PARAM']['SORT'],
            'SMART_FILTER_PATH' => $arParams['SMART_FILTER_PATH']
        ),
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "CONTANER" => '.game-section-wrapper',
        "CONTANER_MORE" => '.game-section-show-more',
        "PAGEN_1" => $arResult["PAGE_NEXT"],
        "SECTION_ID" => (int) $arParams["SECTION_ID"],
        "COUNT" => (int) $arParams["COUNT"],
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        new jsPortalGameSection(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>