<? if (empty($arResult['IS_AJAX'])) { ?>
    <div class="row catalog release-section-wrapper">
    <? } ?>
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <div class="main-release-block">
            <div class="wrap_release-image col-lg-3 col-md-3 col-sm-3 col-xs-12" style="background: url('<?= $arItem['IMG'] ?>') no-repeat;"></div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="release-info">
                    <div class="data"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></div>
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="name"><?= $arItem['NAME'] ?></a>
                    <? if ($arItem['PLATFORMS']) { ?>
                        <div class="release-info_platforms">Платформа
                            <? foreach ($arItem['PLATFORMS'] as $value) { ?>
                                <span class="console"><?= $value ?></span>
                            <? } ?>
                        </div>
                    <? } ?>
                    <? if ($arItem['COUNTRY']) { ?>
                        <div class="country">Где: <span><?= $arItem['COUNTRY'] ?></span></div>
                    <? } ?>
                    <div class="info"><?= $arItem['PREVIEW_TEXT'] ?></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    <? } ?>
    <? if (empty($arResult['IS_AJAX'])) { ?>
    </div>
    <? if ($arResult['PAGE_IS_NEXT']) { ?>
             <div class="wrap_show-more">
            <div class="show-more release-section-show-more">Еще Релизы</div>
        </div>
    <? } ?>
<? } ?>