<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "CONTANER" => '.game-section-mafia-wrapper',
        "CONTANER_RATING" => '.game-section-mafia-number div',
        "CONTANER_MORE" => '.game-section-mafia-show-more',
        "PAGEN_1" => $arResult["PAGE_NEXT"],
        "RATING" => $arParams["RATING"],
        "SECTION_ID" => (int) $arParams["SECTION_ID"],
        "COUNT" => (int) $arParams["COUNT"],
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        var jsRatingUserSection = new jsPortalGameSection(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        jsRatingUserSection.config.reloadRating = function (section) {
            //заглушка
        };
    </script>
<? } ?>