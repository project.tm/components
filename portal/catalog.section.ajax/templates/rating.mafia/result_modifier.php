<?

foreach ($arResult['ITEMS'] as &$arItem) {
    if (!empty($arItem['PROPERTY_PLATFORM_VALUE'])) {
        foreach ($arItem['PROPERTY_PLATFORM_VALUE'] as $key => $value) {
            $name = Igromafia\Game\Property::getPlatform($value);
            if($name) {
                $arItem['PROPERTY_PLATFORM_VALUE'][$key] = $name;
            } else {
                unset($arItem['PROPERTY_PLATFORM_VALUE'][$key]);
            }
        }
    }
    $arItem['RATING_MAFIA'] = $arItem['PROPERTY_RATINGIGROMAFII_VALUE'];

    $img = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'];
    if ($img) {
        $arItem['IMG'] = Igromafia\Game\Image::resize($img, 406, 213);
    } else {
        $arItem['IMG'] = SITE_TEMPLATE_PATH .'/assets/images/game01.png';
    }
}
unset($arItem);

if (empty($arResult['IS_AJAX'])) {
    $arFilter = array(
        "IBLOCK_ID" => $this->arParams['ITEM_IBLOCK_ID'],
        'ACTIVE' => 'Y',
    );
    $arResult['RATING'] = array();
    $res = CIBlockElement::GetList(array(), $arFilter, array('PROPERTY_RATINGIGROMAFII'), false, array('PROPERTY_RATINGIGROMAFII'));
    while ($arItem = $res->GetNext()) {
        $arResult['RATING'][ceil($arItem['PROPERTY_RATINGIGROMAFII_VALUE'])] = true;
    }
}