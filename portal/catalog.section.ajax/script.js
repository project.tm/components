(function (window) {

    if (window.jsPortalGameSection) {
        return;
    }

    window.jsPortalGameSection = function (arParams) {
        var self = this;
        self.config = {
            ajax: arParams.AJAX,
            rating: arParams.CONTANER_RATING,
            contaner: arParams.CONTANER,
            more: arParams.CONTANER_MORE,
            reloadRatingStart: false,
            reloadRatingStop: false
        };
        self.param = {
            IBLOCK_ID: arParams.IBLOCK_ID,
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            RATING: arParams.RATING,
            FILTER_RATING: arParams.FILTER_RATING,
            PARAM: arParams.PARAM,
            PAGEN_1: arParams.PAGEN_1,
            SECTION_ID: arParams.SECTION_ID,
            COUNT: arParams.COUNT
        };

        self.loadAjax = function (t, func, funcStart) {
            t = $(t);
            t.data('html', t.html()).text("Загрузка... ");

            $.get(self.config.ajax, self.param, function (data) {
                if (data) {
                    self.param.PAGEN_1++;
                    if(funcStart) {
                        funcStart(self);
                    }
                    $(self.config.contaner).append(data.content);
                    if (func) {
                        func(self);
                        t.html(t.data('html')).html(t.data('html'));
                        if (data.isNext) {
                            $(self.config.more).removeClass('hidden');
                        } else {
                            $(self.config.more).addClass('hidden');
                        }
                    } else {
                        if (!data.isNext) {
                            t.addClass('hidden');
                        }
                        t.removeClass('active').html(t.data('html')).html(t.data('html'));
                    }
                }
            }, 'json').fail(function () {
                t.text("Не удалось загрузить страницу, попробуйте позже")
                        .delay(1500)
                        .queue(function (n) {
                            $(this).html(t.data('html'));
                            n();
                        });
            });
        };

        $(self.config.more).on('click', function () {
            self.loadAjax(this);
        });
        if (self.config.rating) {
            $(document).on('click', self.config.rating + ':not(.rating-disabled)', function () {
                var newStart = $(this).text();
                if (newStart) {
                    if (self.param.FILTER_RATING != newStart) {
                        self.param.FILTER_RATING = newStart;
                    } else {
                        self.param.FILTER_RATING = 0;
                    }
                } else {
                    self.param.FILTER_RATING = 0;
                }
                if (self.param.FILTER_RATING) {
                    $(this).addClass('active').siblings('.section-rating-number').removeClass('active')
                } else {
                    console.log($(this));
                    $(this).removeClass('active').siblings('.section-rating-number').removeClass('active')
                }

                self.param.PAGEN_1 = 1;
                $(self.config.contaner).empty();
//                self.loadAjax(this, self.config.reloadRatingStop, self.config.reloadRatingStart);
                self.loadAjax(this, self.config.reloadRating);

                return false;
            });

        }
    };
})(window);