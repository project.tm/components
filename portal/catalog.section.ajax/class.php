<?php

use Bitrix\Main\Loader,
    Bitrix\Main\SystemException;

include(__DIR__ . '/filter.php');

class PortalCatalogSection extends CBitrixComponent {

    static private function initIblockGame(&$arParams) {
        $sort = Igromafia\Game\Sort::get($arParams);
        $arParams['RATING_MAFIA'] = $sort['TYPE'] !== 'user';
        $arParams['PARAM']['SORT'] = $sort['TYPE'] ?: '';
        $arParams['ITEM_IBLOCK_ID'] = Igromafia\Game\Config::GAME_IBLOCK;
        if (!empty($arParams['PARAM']['SMART_FILTER_PATH'])) {
            PortalCatalogSectionFilter::init($arParams);
        }
        if ($sort) {
            $request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
            $rating = (int) $request->getQuery('rating');
            if ($rating) {
                $arParams['RATING'] = strtoupper($sort['TYPE']);
                $arParams['FILTER_RATING'] = $rating;
            }
        }
        $arParams['SELECT'] = array('PROPERTY_RATING', 'PROPERTY_RATINGIGROMAFII', 'PROPERTY_PLATFORM');
        if (!empty($arParams['SECTION_ID'])) {
            $arParams['FILTER']['SECTION_ID'] = $arParams['SECTION_ID'];
            $arParams['FILTER']['INCLUDE_SUBSECTIONS'] = 'Y';
            $arParams['FILTER']['SECTION_GLOBAL_ACTIVE'] = 'Y';
        }
        switch ($arParams['RATING']) {
            case 'TOP':
                $arParams['SORT'] = array(
                    'PROPERTY_RATINGIGROMAFII' => 'DESC,NULL',
                    'PROPERTY_RATING' => 'DESC,NULL',
                    'CREATED' => 'DESC'
                );
                break;

            case 'USER':
                if (empty($arParams['FILTER_RATING'])) {
                    $arParams['FILTER'] = array(
                        '>PROPERTY_RATING' => 0
                    );
                } else {
                    $arParams['FILTER_RATING'] = (float) $arParams['FILTER_RATING'];
                    $arParams['FILTER'] = array(
                        '>PROPERTY_RATING' => $arParams['FILTER_RATING'] - 1,
                        '<=PROPERTY_RATING' => $arParams['FILTER_RATING']
                    );
                }
                $arParams['SORT'] = array(
                    $sort ? $sort['KEY'] : 'PROPERTY_RATING' => 'DESC,NULL',
                    'CREATED' => 'DESC',
                );
                break;

            case 'MAFIA':
                if (empty($arParams['FILTER_RATING'])) {
                    $arParams['FILTER'] = array(
                        '>PROPERTY_RATINGIGROMAFII' => 0
                    );
                } else {
                    $arParams['FILTER_RATING'] = (float) $arParams['FILTER_RATING'];
                    $arParams['FILTER'] = array(
                        '>PROPERTY_RATINGIGROMAFII' => $arParams['FILTER_RATING'] - 1,
                        '<=PROPERTY_RATINGIGROMAFII' => $arParams['FILTER_RATING']
                    );
                }
                $arParams['SORT'] = array(
                    $sort ? $sort['KEY'] : 'PROPERTY_RATINGIGROMAFII' => 'DESC,NULL',
                    'CREATED' => 'DESC',
                );
                break;

            default:
                if ($sort) {
                    $arParams['SORT'] = array(
                        $sort['KEY'] => 'DESC,NULL',
                        'CREATED' => 'DESC',
                    );
                }
                break;
        }
    }

    static private function initIblockNews(&$arParams) {
        $iblockMap = array(
            'ARTICLE' => Igromafia\Game\Config::ARTICLE_IBLOCK,
            'REVIEWS' => Igromafia\Game\Config::REVIEWS_IBLOCK,
            'RELEASE' => Igromafia\Game\Config::RELEASE_IBLOCK,
            'NEWS' => Igromafia\Game\Config::NEWS_IBLOCK,
        );
        $arParams['ITEM_IBLOCK_ID'] = $iblockMap[$arParams['IBLOCK_ID']];
        if (!empty($arParams['SECTION_ID'])) {
            $arParams['FILTER']['PROPERTY_GAME'] = $arParams['SECTION_ID'];
        }
        $arParams['SELECT'] = array('PREVIEW_TEXT', 'CREATED_BY', 'GAME');
        if ($arParams['IBLOCK_ID'] == 'RELEASE') {
            $arParams['SELECT'][] = 'PROPERTY_PLATFORM';
            $arParams['SELECT'][] = 'PROPERTY_COUNTRY';
            $arParams['SELECT'][] = 'PROPERTY_DATE';
            $arParams['SELECT'][] = 'PROPERTY_GAME';
            $arParams['SELECT'][] = 'ACTIVE_FROM';
        }
    }

    static private function initIblockParam(&$arParams) {
        $arParams = array_merge(array(
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
                ), $arParams);

        if (empty($arParams['FILTER'])) {
            $arParams['FILTER'] = array();
        }

        if (empty($arParams['COUNT']) or $arParams['COUNT'] < 0 or $arParams['COUNT'] > 50) {
            $arParams['COUNT'] = 6;
        }
//        $arParams['COUNT'] = 1;

        $arParams['SORT'] = array(
            'CREATED' => 'DESC',
        );
        $arParams['SELECT'] = array();
        switch ($arParams['IBLOCK_ID']) {
            case 'GAME':
                self::initIblockGame($arParams);
                break;

            case 'ARTICLE':
            case 'REVIEWS':
            case 'RELEASE':
            case 'NEWS':
                self::initIblockNews($arParams);
                break;

            default :
                throw new SystemException("Ошибка настройки");
                break;
        }
    }

    public function executeComponent() {
        if (Loader::includeModule('igromafia.game')) {
            self::initIblockParam($this->arParams);
            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
            $arNavParams = array(
                "nPageSize" => $this->arParams["COUNT"],
                "bDescPageNumbering" => false,
                "bShowAll" => false,
            );
            $arNavigation = CDBResult::GetNavParams($arNavParams);
            if (!empty($_REQUEST['q'])) {
                $this->arParams['FILTER']['%NAME'] = $_REQUEST['q'];
            }
            if (isset($this->arParams['SET_FILTER'])) {
                global $arrFilter;
                $arrFilter = $this->arParams['FILTER'];
            } else {
                if ($this->startResultCache(false, array($this->arParams, $arNavigation))) {
                    $arFilter = array(
                        "IBLOCK_ID" => $this->arParams['ITEM_IBLOCK_ID'],
                        'ACTIVE' => 'Y',
                    );
                    if (!empty($this->arParams['FILTER'])) {
                        $arFilter = array_merge($arFilter, $this->arParams['FILTER']);
                    }
                    $arSelect = array('ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'DETAIL_PAGE_URL');
                    if (!empty($this->arParams['SELECT'])) {
                        $arSelect = array_merge($arSelect, $this->arParams['SELECT']);
                    }

                    global $arrFilter;
                    if ($arrFilter) {
                        $arFilter = array_merge($arFilter, $arrFilter);
                    }
                    $this->arResult['FILTER'] = $arFilter;

//                pre($this->arParams['SORT'], $arFilter);
                    $res = CIBlockElement::GetList($this->arParams['SORT'], $arFilter, false, $arNavParams, $arSelect);
                    $this->arResult['PAGE_NEXT'] = empty($this->arParams['PAGE']) ? 2 : $this->arParams['PAGE'] ++;
                    $this->arResult['PAGE_COUNT'] = $res->NavPageCount;
                    $this->arResult['PAGE_ITEM'] = $res->NavPageNomer;
                    $this->arResult['PAGE_IS_NEXT'] = $this->arResult['PAGE_ITEM'] < $this->arResult['PAGE_COUNT'];
//                    $res->SetUrlTemplates($this->arParams["DETAIL_URL"]);
                    while ($arItem = $res->GetNext()) {
                        $this->arResult['ITEMS'][] = $arItem;
                    }

                    if ($this->arParams['SECTION_ID']) {
                        $arFilter = array(
                            "IBLOCK_ID" => $this->arParams['ITEM_IBLOCK_ID'],
                            "ID" => $this->arParams['SECTION_ID'],
                            'ACTIVE' => 'Y',
                        );
                        $res = CIBlockSection::GetList(Array(), $arFilter, false, array('NAME'));
                        if ($arItem = $res->GetNext()) {
                            $this->arResult['SECTION'] = $arItem['NAME'];
                        }
                    }
//                global $APPLICATION;
//                $navComponentParameters = array(
//                    'BASE_LINK' => $APPLICATION->GetCurPage(false)
//                );
//                $this->arResult["NAV_STRING"] = $res->GetPageNavStringEx(
//                        $navComponentObject, $this->arParams["PAGER_TITLE"], $this->arParams["PAGER_TEMPLATE"], $this->arParams["PAGER_SHOW_ALWAYS"], $this, $navComponentParameters
//                );
//                $this->arResult["NAV_CACHED_DATA"] = null;
//                $this->arResult["NAV_RESULT"] = $res;
//                $this->arResult["NAV_PARAM"] = $navComponentParameters;

                    $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
                    $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
                    $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
                    $this->SetResultCacheKeys(array(
                        "PAGE_NEXT",
                        "PAGE_IS_NEXT",
                        "PAGE_COUNT",
                        "PAGE_ITEM",
                        "SECTION",
                        "AJAX",
                        "SCRIPT",
                        "TEMPLATE_NAME",
                        "IS_AJAX",
                    ));
                    $this->includeComponentTemplate();
                }
                if (!empty($this->arResult['SECTION'])) {
                    global $APPLICATION;
                    $APPLICATION->AddChainItem($this->arResult['SECTION']);
                }
            }
        }
        return $this->arResult['PAGE_IS_NEXT'] ?: 0;
    }

}
