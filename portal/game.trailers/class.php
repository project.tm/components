<?php

use CIBlockElement,
    CDBResult,
    Bitrix\Main\Loader;

class PortalGameTrailers extends CBitrixComponent {

    public function executeComponent() {
        $arNavParams = array(
            "nPageSize" => $this->arParams["COUNT"] ?: 6,
            "bDescPageNumbering" => false,
            "bShowAll" => false,
        );
        $arNavigation = CDBResult::GetNavParams($arNavParams);
        if ($this->startResultCache(false, array($this->arParams, $arNavigation))) {
            if (Loader::includeModule('iblock')) {
                $this->arResult['TRAILERS'] = array();

                $arSelect = Array("ID", "NAME", "PROPERTY_TRAILERS");
                $arFilter = Array(
                    "IBLOCK_ID" => 5,
                    "ACTIVE" => "Y",
                    "!PROPERTY_TRAILERS" => false
                );
                $res = CIBlockElement::GetList(Array('PROPERTY_VIEWED'=>'DESC'), $arFilter, false, $arNavParams, $arSelect);
                while ($arItem = $res->GetNext()) {
                    $this->arResult['TRAILERS'][] = $arItem;
                }

                $this->arResult['PAGE_COUNT'] = $res->NavPageCount;
                $this->arResult['PAGE_ITEM'] = $res->NavPageNomer;
                $this->arResult['PAGE_IS_NEXT'] = $this->arResult['PAGE_ITEM'] < $this->arResult['PAGE_COUNT'];

                $this->includeComponentTemplate();
            }
        }
    }

}
