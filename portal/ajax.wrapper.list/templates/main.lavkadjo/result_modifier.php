<?

if (Bitrix\Main\Loader::includeModule('igromafia.game')) {
    $arResult['PLATFORM'] = Project\Core\Utility::useCache(array('main.lavkadjo', 'platform'), function() {
                $arResult = array();
                $arFilter = array(
                    'IBLOCK_ID' => Igromafia\Game\Config::DZHO_IBLOCK,
                    '!PROPERTY_PLATFORM' => false,
                    'ACTIVE' => 'Y'
                );
                $arSelect = array(
                    'PROPERTY_PLATFORM'
                );
                $res = CIBlockElement::GetList(array(), $arFilter, array('PROPERTY_PLATFORM'), false, $arSelect);
                while ($arItem = $res->GetNext()) {
                    $name = Igromafia\Game\Property::getPlatform($arItem['PROPERTY_PLATFORM_VALUE']);
                    if ($name) {
                        $arResult[$arItem['PROPERTY_PLATFORM_VALUE']] = array(
                            'NAME' => $name
                        );
                    }
                }
                return $arResult;
            });
    $arResult['PLATFORM']['']['NAME'] = 'Все платформы';

    foreach ($arResult['PLATFORM'] as $key => $value) {
        if ($arParams['FILTER'] == $key) {
            $arResult['PLATFORM'][$key]['SELECTED'] = true;
            if ($key) {
                global $arrFilterUser;
                $arrFilterUser = array(
                    'PROPERTY_PLATFORM' => $key
                );
            }
        }
    }
}