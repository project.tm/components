<?

if (Bitrix\Main\Loader::includeModule('iblock')) {
    $arSelect = Array("ID");
    $arFilter = Array(
        "IBLOCK_ID" => 14,
        "ACTIVE" => "Y",
        "PROPERTY_SELLER" => $arResult["PARAM"]
    );
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if ($arItem = $res->GetNext()) {
        $arResult['IS_SHOP'] = true;
    }
}