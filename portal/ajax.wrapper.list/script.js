(function (window) {
    'use strict';

    if (window.jsPortalAjaxWrapperList) {
        return;
    }

    window.jsPortalAjaxWrapperList = function (arParams) {
        let self = this;
        self.config = {
            ajax: arParams.AJAX,
            contaner: arParams.CONTANER,
            contanerList: arParams.CONTANER_LIST,
            contanerMore: arParams.CONTANER_MORE,
            contanerFilter: arParams.CONTANER_FILTER + ' .filter-element',
            contanerFilterAll: arParams.CONTANER_FILTER_ALL + ' .filter-element-all',
            start: false,
            stop: false
        };

        self.param = {
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            FILTER: '',
            PAGEN_1: 2,
            IS_UPDATE: 0,
            PARAM: arParams.PARAM
        };

        $(document).on('click', self.config.contanerMore, function () {
            self.loadAjax(this);
        });

        $(document).on('click', self.config.contanerFilter, function () {
            let param = $(this).data('filter');
            if (self.param.FILTER != param) {
                $(self.config.contanerFilter).each(function () {
                    if (param == $(this).data('filter')) {
                        $(this).addClass('active');
                    } else {
                        $(this).removeClass('active');
                    }
                });
                self.param.FILTER = param;
                self.update(this);
            }
        });
    };

    window.jsPortalAjaxWrapperList.prototype.update = function (el) {
        this.param.PAGEN_1 = 1;
        this.param.IS_UPDATE = 1;
        this.loadAjax(el);
    };

    window.jsPortalAjaxWrapperList.prototype.showNext = function (is, hidden) {
        if (!hidden) {
            hidden = 'hidden';
        }
        console.log(is);
        if (is) {
            $(this.config.contanerMore).parent().removeClass(hidden);
        } else {
            $(this.config.contanerMore).parent().addClass(hidden);
        }
    };

    window.jsPortalAjaxWrapperList.prototype.loadAjax = function (el) {
        let self = this;
        el = $(el);
        el.data('html', el.html()).text("Загрузка... ");

        $.get(self.config.ajax, self.param, function (data) {
            if (data) {
                self.param.PAGEN_1++;
                if (self.start) {
                    self.start(el);
                }
                if (self.param.IS_UPDATE) {
                    $(self.config.contaner).html(data.content);
                } else {
                    $(self.config.contanerList).append(data.content);
                }
                
                $(self.config.contanerList).each(function() {
                    $(this).find('.lavka-match-height__item--js').matchHeight({
                        byRow: true
                    });
                });

                if (self.stop) {
                    self.stop(el);
                    el.html(el.data('html')).html(el.data('html'));
                } else {
                    el.removeClass('active').html(el.data('html')).html(el.data('html'));
                }
            }
            self.param.IS_UPDATE = 0;
        }, 'json').fail(function () {
            el.text("Не удалось загрузить страницу, попробуйте позже")
                    .delay(1500)
                    .queue(function (n) {
                        $(this).html(el.data('html'));
                        n();
                    });
        });
    };
})(window);