<?php

use CIBlockElement,
    CDBResult,
    Bitrix\Main\Loader;

class PortalPersonalPublication extends CBitrixComponent {

    public function executeComponent() {
        $arNavParams = array(
            "nPageSize" => $this->arParams["COUNT"] ?: 6,
            "bDescPageNumbering" => false,
            "bShowAll" => false,
        );
        $arNavigation = CDBResult::GetNavParams($arNavParams);
        if ($this->startResultCache(false, array($this->arParams, $arNavigation))) {
            if (Loader::includeModule('iblock')) {
                $this->arResult['PUBLICATION'] = array();

                $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
                $arFilter = Array(
                    "IBLOCK_ID" => array(1, 4),
                    "ACTIVE" => "Y",
                    "CREATED_BY" => $this->arParams['USER_ID']
                );
                $res = CIBlockElement::GetList(Array(), $arFilter, false, $arNavParams, $arSelect);
                while ($arItem = $res->GetNext()) {
                    switch ($arItem['IBLOCK_CODE']) {
                        case 'news':
                            $arItem['IBLOCK_CODE_NAME'] = 'новости';
                            break;
                        case 'articles':
                            $arItem['IBLOCK_CODE_NAME'] = 'статьи';
                            break;
                        default:
                            $arItem['IBLOCK_CODE_NAME'] = 'новости';
                    }
                    $this->arResult['PUBLICATION'][] = $arItem;
                }

                $this->arResult['PAGE_COUNT'] = $res->NavPageCount;
                $this->arResult['PAGE_ITEM'] = $res->NavPageNomer;
                $this->arResult['PAGE_IS_NEXT'] = $this->arResult['PAGE_ITEM'] < $this->arResult['PAGE_COUNT'];

                if ($this->arResult['PUBLICATION']) {
                    $this->includeComponentTemplate();
                }
            }
        }
    }

}
