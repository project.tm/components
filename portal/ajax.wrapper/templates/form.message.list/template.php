<?

if (Bitrix\Main\Loader::includeModule('project.support')) {
    if (is_numeric($arResult['PARAM'])) {
        $arGame = Project\Support\JoShop::getById($arResult['PARAM']);
        $APPLICATION->IncludeComponent("project.support:form.message", 'list', array(
            'TYPE' => $arGame['SELLER_ID'],
            'GAME' => $arGame,
            'FORM_SELECT' => $arResult['FORM_SELECT']
        ));
    } else {
        $APPLICATION->IncludeComponent("project.support:form.support", 'list', array(
            'THEME' => Project\Support\Theme::getHeader($arResult['PARAM']),
            'TYPE' => $arResult['PARAM'],
            'FORM_SELECT' => $arResult['FORM_SELECT']
        ));
    }
}
