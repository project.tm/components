<?

$iblockMap = Igromafia\Game\Config::getIblocName();
foreach ($arResult["ITEMS"] as &$arItem) {
    $arItem['TYPE'] = $iblockMap[$arItem['IBLOCK_ID']];
}

//pre($iblockMap);

if (Bitrix\Main\Loader::includeModule('igromafia.game')) {
    $arResult['FAVORITES'] = Project\Core\Utility::useCache(array('favorites.game', 'type', cUser::GetID()), function() use($arParams) {
                $arResult = array(
                    '' => array(
                        'NAME' => 'ВСЕ'
                    )
                );
                $param = array(
                    'select' => array(
                        'UF_ENTITY',
                    ),
                    'filter' => array(
                        'UF_ENTITY' => $arParams['FILTER_ENTITY'],
                        'UF_USER' => cUser::GetID(),
                        'ELEMENT.ACTIVE' => 'Y'
                    ),
                    'runtime' => array(
                        'ELEMENT' => array(
                            'data_type' => '\Bitrix\Iblock\ElementTable',
                            'reference' => array(
                                '=this.UF_ID' => 'ref.ID',
                                '=this.UF_XML_ID' => 'ref.IBLOCK_ID'
                            ),
                            'join_type' => 'inner'
                        ),
                    ),
                );
                $rsData = Igromafia\Game\Model\FavoritTable::getList($param);
                $rsData = new CDBResult($rsData);
                while ($arItem = $rsData->Fetch()) {
                    $arResult[$arItem['UF_ENTITY']] = $arItem['UF_ENTITY'];
                }
                return $arResult;
            });

    foreach ($iblockMap as $key => $value) {
        $type = strtoupper($value['class']);
        if (isset($arResult['FAVORITES'][$type])) {
            $arResult['FAVORITES'][$type] = array(
                'NAME' => $value['name-header']
            );
        }
    }

    foreach ($arResult['FAVORITES'] as $key => $value) {
        if ($arParams['FILTER'] == $key) {
            $arResult['FAVORITES'][$key]['SELECTED'] = true;
        }
    }
}