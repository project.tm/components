<?

$elements = array();
foreach ($arResult["ITEMS"] as &$arItem) {
    $elements[] = $arItem['ID'];
    $img = empty($arItem['PREVIEW_PICTURE']['ID']) ? $arItem['DETAIL_PICTURE']['ID'] : $arItem['PREVIEW_PICTURE']['ID'];
    if ($img) {
        $arItem['IMG'] = Igromafia\Game\Image::resize($img, 127, 98);
    } else {
        $arItem['IMG'] = SITE_TEMPLATE_PATH . '/assets/images/user03.png';
    }
}

$rsData = Igromafia\Game\Model\FavoritTable::getList(array(
            'select' => array('ID'),
            'filter' => array(
                'UF_ENTITY' => 'COLLECTION',
                'UF_USER' => $GLOBALS['USER']->GetID(),
                'UF_ID' => $elements
            ),
        ));
if ($rsData = new CDBResult($rsData)) {

}

$arResult["DZHO"] = array();
$arSelect = Array("ID", "DETAIL_PAGE_URL", 'PROPERTY_GAME_FROM_PORTAL');
$arFilter = Array(
    "IBLOCK_ID" => Igromafia\Game\Config::DZHO_IBLOCK,
    "ACTIVE" => "Y",
    'PROPERTY_GAME_FROM_PORTAL' => $elements,
    'PROPERTY_SELLER' => $GLOBALS['USER']->GetID()
);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while ($arItem = $res->GetNext()) {
    $arResult["DZHO"][$arItem['PROPERTY_GAME_FROM_PORTAL_VALUE']] = $arItem['DETAIL_PAGE_URL'];
}