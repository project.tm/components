<? if ($arResult['IS_FILL']) { ?>
    <?
    $first = $arResult['MESSAGE'][0];
    $last = $arResult['MESSAGE'][1] ?: 0;
    unset($arResult['MESSAGE'][0], $arResult['MESSAGE'][1]);
    ?>
    <div class="remove-icon" data-id="<?= $arParams['THEME_ID'] ?>"></div>
    <div class="edit-game"><span class="edit-game-message" data-game="<?= $arResult['INTERLOCUTOR'] ?>">ответить</span>
        <? if($arResult["POST"]>2) {?>
            <span class="trigger">Вся переписка (<?= $arResult["POST"] ?>)</span>
        <? } ?>
    </div>
    <div class="wrap-icon">
        <a href="<?= $first['AUTHOR']['URL'] ?>">
            <div class="icon" style="background:url('<?= $first['AUTHOR']['PERSONAL_PHOTO_MEDIUM'] ?>');"></div>
            <div class="txt"><?= $first['AUTHOR']['NAME'] ?></div>
        </a>
    </div>
    <div class="message-body">
        <div class="theme"><?= $arResult['THEME'] ?> <? if (isset($arResult['GAME'])) { ?><a href="<?= $arResult['GAME']['URL'] ?>"><?= $arResult['GAME']['NAME'] ?></a><? } ?></div>
        <div class="content-block">
            <?= $first['POST_MESSAGE_TEXT'] ?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="messages-block-wrapper">
        <? if ($last) { ?>
            <div class="comment-block">
                <a href="<?= $last['AUTHOR']['URL'] ?>">
                    <div class="u-icon" style="background:url('<?= $last['AUTHOR']['PERSONAL_PHOTO_SMALL'] ?>');"><span class="you"><?= $last['AUTHOR']['NAME'] ?></span></div>
                </a>
                <div class="u-comment">
                    <?= $last['POST_MESSAGE_TEXT'] ?>
                </div>
                <div class="clear"></div>
            </div>
        <? } ?>
        <div class="messages-block-wrapper-last">
            <div class="messages-block-wrapper-comment">
            <? } ?>
            <? foreach ($arResult['MESSAGE'] as $arItem) { ?>
                <div class="comment-block">
                    <a href="<?= $arItem['AUTHOR']['URL'] ?>">
                        <div class="u-icon" style="background:url('<?= $arItem['AUTHOR']['PERSONAL_PHOTO_SMALL'] ?>');"><span class="you"><?= $arItem['AUTHOR']['NAME'] ?></span></div>
                    </a>
                    <div class="u-comment">
                        <?= $arItem['POST_MESSAGE_TEXT'] ?>
                    </div>
                    <div class="clear"></div>
                </div>
            <? } ?>
            <? if ($arResult['IS_FILL']) { ?>
            </div>
            <? if ($arResult['PAGE_IS_NEXT']) { ?>
                <div class="wrap_show-more">
                    <div class="show-more">Показать еще </div>
                </div>
            <? } ?>
        </div>
    </div>
<? } ?>
<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "THEME_ID" => $arResult["THEME_ID"],
        "MORE" => '.show-more',
        "COMMENT" => '.messages-block-wrapper-comment',
        "CONTANER" => '.messages-block-' . $arResult['THEME_ID'],
    );
    ?>
    <script>
        portalForumMessageList[<?= $arResult['THEME_ID'] ?>] = new jsPortalForumMessageList(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>