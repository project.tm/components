<? if (empty($arResult['IS_AJAX'])) { ?>
    <div class="message-list-contaner">
    <? } ?>
    <?
    if($arResult["TOPIC"]) {
    $APPLICATION->IncludeComponent(
            "project.support:forum.topic.last", "messages", array(
        "PAGER_BASE_LINK_ENABLE" => 'Y',
        "PAGER_BASE_LINK" => '/personal/message/',
        "TOPIC_LIST" => $arResult["TOPIC"],
        "ALLOW_ANCHOR" => "N",
        "ALLOW_BIU" => "N",
        "ALLOW_CODE" => "N",
        "ALLOW_FONT" => "N",
        "ALLOW_HTML" => "N",
        "ALLOW_IMG" => "N",
        "ALLOW_LIST" => "N",
        "ALLOW_NL2BR" => "N",
        "ALLOW_QUOTE" => "N",
        "ALLOW_SMILES" => "N",
        "ALLOW_TABLE" => "N",
        "ALLOW_VIDEO" => "N",
        "CACHE_TAG" => "Y",
        "CACHE_TIME" => "0",
        "CACHE_TYPE" => "N",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
        "FID" => Project\Support\Config::FORUMS,
        "IMAGE_SIZE" => "600",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "users",
        "PAGER_TITLE" => "Темы",
        "SEPARATE" => "в форуме #FORUM#",
        "SET_NAVIGATION" => "Y",
        "SET_TITLE" => "N",
        "SHOW_COLUMNS" => array(
        ),
        "SHOW_FORUM_ANOTHER_SITE" => "N",
        "SHOW_NAV" => array(
            0 => "BOTTOM",
        ),
        "SHOW_SORTING" => "N",
        "SHOW_TOPIC_POST_MESSAGE" => "NONE",
        "SORT_BY" => "LAST_POST_DATE",
        "SORT_BY_SORT_FIRST" => "N",
        "SORT_ORDER" => "DESC",
        "TOPICS_PER_PAGE" => "3",
        "TOPIC_POST_MESSAGE_LENGTH" => "0",
        "URL_TEMPLATES_INDEX" => "",
        "URL_TEMPLATES_LIST" => "",
        "URL_TEMPLATES_MESSAGE" => "#TID#/",
        "URL_TEMPLATES_PROFILE_VIEW" => "",
        "URL_TEMPLATES_READ" => "post#FID#-#TID#/",
        "COMPONENT_TEMPLATE" => "messages"
            ), false
    );
    } else {
        ?><div class="personal-list-empty">Нет сообщений</div><?
    }
    ?>
    <? if (empty($arResult['IS_AJAX'])) { ?>
    </div>
    <? } ?>