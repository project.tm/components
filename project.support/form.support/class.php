<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Project\Support\Form,
    Project\Support\Message,
    Project\Support\Theme,
    Project\Support\Config;

class PortalFormSupport extends CBitrixComponent {

    protected function filterForm(&$arData) {
        global $USER;
        $request = Application::getInstance()->getContext()->getRequest();
        if (empty($request->getPost('MESSAGE'))) {
            $this->arResult['ERROR']['MESSAGE'] = 'Поле «Вопрос» не заполнено';
        } else {
            $arData['MESSAGE'] = $request->getPost('MESSAGE');
        }
        if ($this->arResult['IS_USER']) {
            $arData['EMAIL'] = $USER->GetEmail();
        } elseif (empty($request->getPost('EMAIL'))) {
            $this->arResult['ERROR']['EMAIL'] = 'Поле «E-mail» не заполнено';
        } else {
            $arData['EMAIL'] = $request->getPost('EMAIL');
        }
    }

    protected function sendForm($arData) {
        if ($this->arResult['IS_USER']) {
            $arData['THEME'] = $this->arParams['THEME'];
            $arData['URL'] = Message::add($this->arParams['TYPE'], $arData);
        }
        return Form::add($this->arParams['TYPE'], $arData);
    }

    public function executeComponent() {
        if (Loader::includeModule('project.support')) {
            $request = Application::getInstance()->getContext()->getRequest();
            $this->arResult['IS_USER'] = cUser::IsAuthorized();
            $this->arResult['FORM_SEND'] = $this->GetTemplateName();
            $this->arResult['IS_AJAX'] = ($request->isPost() and check_bitrix_sessid() and $request->getPost('form-send') === $this->arResult['FORM_SEND']);
            $this->arResult['ERROR'] = array();
            if ($this->arResult['IS_AJAX']) {
                $arData = array();
                $this->filterForm($arData);
                if (empty($this->arResult['ERROR'])) {
                    if ($this->sendForm($arData)) {
                        $this->arResult['IS_SEND'] = true;
                        return $this->includeComponentTemplate();
                    }
                    global $strError;
                    $this->arResult['ERROR']['FORM'] = $strError ?: 'Возникли проблемы на сервере, попробуйте повторить позднее.';
                }
                $this->arResult['DATA'] = $arData;
            }
            $this->includeComponentTemplate();
        }
    }

}
