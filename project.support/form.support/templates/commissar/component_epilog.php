<script>
    var formSupportCommissarConfig = {
        rules: {
            MESSAGE: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            MESSAGE: {
                required: "Заполните поле",
                minlength: "Вопрос должен содержать не менее 5 символов"
            }
        }
    };
<? if (empty($this->arResult['IS_USER'])) { ?>
        formSupportCommissarConfig.rules.EMAIL = {
            required: true,
            email: true
        };
        formSupportCommissarConfig.messages.EMAIL = {
            required: "Заполните поле",
            email: "Укажите корректны E-mail"
        };
<? } ?>
</script>
