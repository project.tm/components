$(document).ready(function () {
    $('.formBlock .subscribe').submit(function () {

        var form = $(this).closest('.formBlock .subscribe');
        $.post(form.attr('data-url'), {ajax: 'subscribe', email: form.find('.email').val()}, function (msg) {
            form.find('.subscribe-message').html(msg).show();
            console.log(form.find('.subscribe-message'), msg);
        });
        return false;
    });
});