<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<section class="formBlock">
    <div class="wrapper">
        <form class="subscribe"
             data-url="<?=$arResult['RUN_URL'] ?>">
            <h2>Подписаться на наши новости и скидки</h2>
            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/subscribe.php"), false); ?>
            <div class="subscribe-message"></div>
            <input type="email" class="email" required="true">
            <button type="submit" class="href subscribe-signup">
                Подписаться <img src="<?= SITE_TEMPLATE_PATH ?>/images/svg/hrefArrowWhite.svg" alt="">
            </button>
        </form>
    </div><!-- end wrapper  -->
</section>