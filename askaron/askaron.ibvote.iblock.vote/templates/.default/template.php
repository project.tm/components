<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $frame = $this->createFrame()->begin(); ?>
<div class="user-rating-js">
    <div class="top-text">
        <span>иГРАЛ? оЦЕНИ!</span>
        <div class="the-game"><?= $arResult['NAME'] ?></div>
    </div>
    <div class="rating-content" style="background-image: url('<?= $arResult['IMG'] ?>');">
        <div class="dark-wrap-bg">
            <div class="main-wrap">
                <? if ($arResult['VOTE_COUNT']) { ?>
                    <div class="clicked-users"><?= $arResult['VOTE_COUNT'] ?> <?= $arResult['VOTE_COUNT_TEXT'] ?></div>
                <? } ?>
                <? if ($arResult['RATING']) { ?>
                    <div class="all-rating"><?= $arResult['RATING'] ?></div>
                <? } ?>
                <div class="rating-block">
                    <!-- рейтинг закидывать в data-rating-->
                    <div class="img-mask-off" data-vote-count="<?= $arResult['VOTE_COUNT'] ?>" data-vote-sum="<?= $arResult['VOTE_SUM'] ?>" data-element="<?= $arResult['ID'] ?>" data-user-vote="0" data-rating= "<?= $arResult['RATING'] ?: $arResult['RATING'] ?>">
                        <? for ($i = 1; $i <= 10; $i++) { ?>
                            <a class="rating-btn btn<?= $i ?>">
                                <div class="gray-img"></div>
                            </a>
                        <? } ?>
                    </div>
                    <div class="img-mask"></div>
                </div>
                <a class="vote"><?= $arResult['VOTE'] ? 'переголосовать' : 'проголосовать' ?></a>
            </div>
        </div>
    </div>
    <div class="iblock-vote">
        <form method="post" action="<?= POST_FORM_ACTION_URI ?>" class="vote-form <?= $arParams['FORM_SELECT'] ?>">
            <input name="rating">
            <? echo bitrix_sessid_post(); ?>
            <input type="hidden" name="AJAX_CALL" value="Y" />
            <input type="hidden" name="back_page" value="<?= $arResult["BACK_PAGE_URL"] ?>" />
            <input type="hidden" name="vote_id" value="<?= $arResult["ID"] ?>" />
            <input type="hidden" name="vote" value="<?= GetMessage("T_IBLOCK_VOTE_BUTTON") ?>" />
            <input type="submit" name="submit" value="<?= GetMessage("T_IBLOCK_VOTE_BUTTON") ?>" />
        </form>
    </div>
</div>
<script>
    $(function () {
        initVoteRating();
    });
</script>
<? $frame->end(); ?>