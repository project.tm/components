<?

$arResult['RATING'] = round($arResult['PROPERTY_RATING_VALUE'], 2);
if (CModule::IncludeModule("askaron.ibvote")) {
    $event = new CAskaronIbvoteEvent;
    $arResult['USER'] = $event->getByUser($arResult['ID']);
}
