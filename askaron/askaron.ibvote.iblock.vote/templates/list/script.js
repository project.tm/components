$(function () {
    $(document).on('click', '.rat .vote-show', function () {
        $(this).closest('.rat').find('.vote-item').addClass('hidden');
        $(this).closest('.rat').find('.vote-form').removeClass('hidden');
    });
    
    $(document).on('submit', '.rat .vote-form', function () {
        var rating = parseInt($(this).find('input[name="rating"]'));
        return rating>0;
    });
});